# Data Structures Practice

- Combination of Test Driven files (see below) and self study, using Udemy course
- Udemy Course:
  https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/

[protip: Udemy has sales regularly, don't buy courses for full price]

## Data Structures Study

- [Linked List](./LINKED_LIST_js_udemy/)
- [Queues](./QUEUES_js_udemy/)
- [Stacks](./STACKS_js_udemy/)
- [Trees](./TREES_js_udemy/)
- [Heaps](./HEAPS_js_udemy/)
- [Priority Queues](./PRIORITY_QUEUE_js_udemy/)
- [Hash Tables aka Hash Maps aka Dictionaries aka ...](./HASH_TABLES_js_udemy/)

## Data Structures with Test Driven functionality:

- [Linked Queue](./linked_queue_py/README.md)
- [Linked List](./linked_list_py/README.md)
- [Circular Queue](./circular_queue_py/README.md)

## How to use this repository:

1. Fork and clone it
2. Change the working directory in your terminal to the
   directory that contains the cloned repository
3. Create and activate a virtual Python environment
4. Install the requirements from the _requirements.txt_ file


## Sorting Algo
- [Sorting Algorithms](./SORING_ALGOS_js_udemy/)
- Bubble Sort

