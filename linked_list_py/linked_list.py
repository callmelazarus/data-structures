# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

from ast import Index


class LinkedList:
  def __init__(self, head=None, tail=None):
    self.head = head
    self.tail = tail
    self.length = 0
  
  def insert(self, value, index=None):
    node = LinkedListNode(value)
    # if list is empty
    if self.head == None: 
      self.head = node
      self.tail = node
      self.length += 1


    # need to set the tail's link to the node (it is by default set to None)
    elif (index == self.length):
      self.tail.link = node
      self.tail = node
      self.length += 1

      # insert before head
    elif index == 0:
      node.link = self.head
      self.head = node
      self.length += 1

    # insert between two nodes
    else:
      this_idx_node = self.traverse(index - 1)
      next_idx_node = this_idx_node.link

      node.link = next_idx_node
      this_idx_node.link = node

      self.length += 1



  def get(self, index):
    # return the node based on the index
    # the index passed will be the guide.
    # if index == 0 -> return head
    # if index == 0:
    #   return self.head.value

    # need to raise index_error for indeces >= list.length
    if index + 1 > self.length:
      raise IndexError('Index is beyond length of the list')
    else:
      # start from head, then go from node to node
      i = 0
      _current_node = self.head
      while i < index:
        # continuing to link from the _current_node
        _current_node = _current_node.link
        i += 1
      # return the value
      return _current_node.value

  def traverse(self, index):
      if index >= self.length:
          raise IndexError("index out of range")
      node = self.head
      i = 0
      while i < index:
          node = node.link
          i += 1
      return node


class LinkedListNode:
  def __init__(self, value, link=None):
    self.value = value
    self.link = link

# - tests ----------

test = LinkedList()
test.insert(0.5,0)
test.insert(10,1)
test.insert(20,2)
test.insert(30,3)
test.insert(40,4)
test.insert(50,5)

# print(test.head.value)