/* BFS steps - iterative
Create a queue, and a variable to store the values of nodes visited
Place the root node in the queue

Loop as long as there is anything in the queue

- Dequeue a node from queue, and the push the value of the node into the variables that store the nodes
- if there is a left property on the dequeued node - add to queue
- if there is a right property on the dequeued node - add to the queue

             10
          6      15
      3     8        20

Some steps
queue: [10] * consider queue like a todo list
visited: []

queue: [6, 15]
viisted [10]
*/

class Node {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  insert(value) {
    let newNode = new Node(value);
    if (this.root === null) this.root = newNode;
    else {
      let cur = this.root;
      while (true) {
        if (value === cur.value) return null;
        if (value < cur.value) {
          if (cur.left === null) {
            cur.left = newNode;
            return this;
          } else {
            cur = cur.left;
          }
        } else if (value > cur.value) {
          if (cur.right === null) {
            cur.right = newNode;
            return this;
          } else {
            cur = cur.right;
          }
        }
      }
    }
  }

  find(value) {
    if (!this.root) return false;
    let cur = this.root;
    let found = false;
    while (!found && cur) {
      if (value < cur.value) {
        cur = cur.left;
      } else if (value > cur.value) {
        cur = cur.right;
      } else {
        found = true;
      }
    }
    if (!found) return found;
    return cur;
  }

  bfs() {
    let q = [];
    let visited = [];
    let node = this.root;
    q.push(node);

    while (q.length > 0) {
      // add to end, shift from beginning
      node = q.shift();
      visited.push(node.value);
      if (node.left) {
        q.push(node.left);
      }
      if (node.right) {
        q.push(node.right);
      }
    }
    return visited;
  }
}

let tree = new BinarySearchTree();
tree.insert(10);
tree.insert(6);
tree.insert(3);
tree.insert(8);
tree.insert(15);
tree.insert(20);

// BFS -> should return [10, 6, 15, 3, 8, 20]
