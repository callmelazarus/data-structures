/* DFS steps - recursive - using stacks

PostOrder - bottom up approach
we explore all children, before visiting the node

Very similar to PreOrder, operations are the same, but in different order.
Visiting occurs, and then pushing occurs

create a variable to store the visited nodes
store the root of BST in a variable cur
write a helper function which accepts a node

- if node has left property, call helper fxn with the left property on node
- if node has right property, call helper fxn with right property of node
- push the value of the node to the variable that stores values
- invoke the helper fxn with current value

return visited array

             10
          6      15
      3     8        20

DFS Post Order -> [3, 8, 6, 20, 15, 10]
*/

class Node {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  insert(value) {
    let newNode = new Node(value);
    if (this.root === null) this.root = newNode;
    else {
      let cur = this.root;
      while (true) {
        if (value === cur.value) return null;
        if (value < cur.value) {
          if (cur.left === null) {
            cur.left = newNode;
            return this;
          } else {
            cur = cur.left;
          }
        } else if (value > cur.value) {
          if (cur.right === null) {
            cur.right = newNode;
            return this;
          } else {
            cur = cur.right;
          }
        }
      }
    }
  }

  find(value) {
    if (!this.root) return false;
    let cur = this.root;
    let found = false;
    while (!found && cur) {
      if (value < cur.value) {
        cur = cur.left;
      } else if (value > cur.value) {
        cur = cur.right;
      } else {
        found = true;
      }
    }
    if (!found) return found;
    return cur;
  }

  dfsPostorder() {
    let visited = [];
    let cur = this.root;
    function traverse(node) {
      if (node.left) {
        traverse(node.left);
      }
      if (node.right) {
        traverse(node.right);
      }
      visited.push(node.value)
    }
    traverse(cur)
    return visited
  }
}

let tree = new BinarySearchTree();
tree.insert(10);
tree.insert(6);
tree.insert(3);
tree.insert(8);
tree.insert(15);
tree.insert(20);
tree.dfsPostorder();
