class Node {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  /* INSERT - pass value in correct location in BST

  create a new node
  start at the root
  check if root
  - if no root - root becomes new node
  
  if there is a root - check if val of new node is > or < root

  - if greater
    check to see if there is a node to the RIGHT.
    if there is -> move to THAT node, and repeat
    if not - the node is at the RIGHT property

  - if less
    check to see if there is a node to LEFT
      if there is -> move to that node, and repeat
      if not - node is the LEFT property
    return the BST

      10
    5    13
  2  6  11  20
  */
  insert(value) {
    let newNode = new Node(value);
    if (this.root === null) this.root = newNode;
    // bulk of the code
    else {
      // initialize the current node, and will update
      let cur = this.root;
      // loop always - until we break with a return
      while (true) {
        if (value === cur.value) return null;
        if (value < cur.value) {
          if (cur.left === null) {
            cur.left = newNode;
            return this;
          } else {
            cur = cur.left;
          }
        } else if (value > cur.value) {
          if (cur.right === null) {
            cur.right = newNode;
            return this;
          } else {
            cur = cur.right;
          }
        }
      }
    }
  }

  /* FIND - finding elements within a BST

  start at root
  - check if root. If not, we are done searching
  if root
    - check if value of new node is value we are looking for
      if we found it, we are done
    - if NOT -> check to see if value is > or < value of root

    if greater
      - check to see if there is node on right
        if exists -> move to that node, and repeat
        if not, we are done searching
    if less
      - check if there is node on left
        if exists -> move to that node, and repeat
        if not, we are done searching
  */

  find(value) {
    if (!this.root) return false;
    let cur = this.root;
    let found = false;
    // while we haven't found it, and we have cur
    while (!found && cur) {
      if (value < cur.value) {
        cur = cur.left;
      } else if (value > cur.value) {
        cur = cur.right;
      } else {
        found = true;
      }
    }
    // indicate case where value not found
    if(!found) return found
    return cur;
  }
}

let tree = new BinarySearchTree();
tree.insert(10);
tree.insert(5);
tree.insert(13);
tree.insert(11);
tree.insert(2);
tree.insert(16);
