/* DFS steps - recursive - using stacks

PREORDER - start at root, and then go down to a leaf, before traversing sibling nodes

- create a variable to stores nodes visited
- store the root of the BST in current

- Write a helper function that accepts a node
  - push the value of the node to the variable that stores the vlues
  - if node has left property, call the helper fxn with the left property
  - if node has right property, call the helper fxn with the right property

- invoke helper function with current variable
- return the array of values

             10
          6      15
      3     8        20

DFS Pre Order -> [10, 6, 3, 8, 15, 20]

*/

class Node {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  insert(value) {
    let newNode = new Node(value);
    if (this.root === null) this.root = newNode;
    else {
      let cur = this.root;
      while (true) {
        if (value === cur.value) return null;
        if (value < cur.value) {
          if (cur.left === null) {
            cur.left = newNode;
            return this;
          } else {
            cur = cur.left;
          }
        } else if (value > cur.value) {
          if (cur.right === null) {
            cur.right = newNode;
            return this;
          } else {
            cur = cur.right;
          }
        }
      }
    }
  }

  find(value) {
    if (!this.root) return false;
    let cur = this.root;
    let found = false;
    while (!found && cur) {
      if (value < cur.value) {
        cur = cur.left;
      } else if (value > cur.value) {
        cur = cur.right;
      } else {
        found = true;
      }
    }
    if (!found) return found;
    return cur;
  }

  dfsPreorder() {
    let visited = [];
    let cur = this.root;

    function traverse(node) {
      visited.push(node.value);
      if (node.left) {
        traverse(node.left);
      }
      if (node.right) {
        traverse(node.right);
      }
    }
    traverse(cur);
    return visited;
  }
}

let tree = new BinarySearchTree();
tree.insert(10);
tree.insert(6);
tree.insert(3);
tree.insert(8);
tree.insert(15);
tree.insert(20);
tree.dfsPreorder()
