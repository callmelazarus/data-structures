class MaxBinaryHeap {
  constructor() {
    this.values = [41, 39, 33, 18, 27, 12];
  }

  /* INSERT - adding to the end, and bubble up
Recall, the largest value is at the root for max binary heap

Bubble up:
Compare the added_child with parent to see if child is larger
Swap if added_child is larger

PseudoCode:
Push the value into the value property of the heap
bubble up the value to correct spot:
  - Create a variable called index, which is the length of the value property -1
  - Create variable called parentIndex, which is the floor of (index-1)/2
  - keep looping as long as the value element at the parentIndex is less than the values element of the child index
    - swap the value of the values element at the parentIndex with the value of the element property at the child index
    - set the index to be the parentIndex, and start over!

    [41, 39, 33, 18, 27, 12, 55]
      0   1   2   3   4   5   6

  */
  insert(element) {
    this.values.push(element);
    this.bubbleUp();
    console.log(this.values);
  }

  bubbleUp() {
    let index = this.values.length - 1;
    const element = this.values[index];
    // stop once the index goes below 0
    while (index > 0) {
      let parentIndex = Math.floor((index - 1) / 2);
      let parent = this.values[parentIndex];
      if (element <= parent) break;

      this.values[parentIndex] = element;
      this.values[index] = parent;
      // need to update the index with the parentindex, to now check if that should be bubbled up as well
      index = parentIndex;
    }
  }

  /* REMOVE or extractMax - extracting the root

PROCESS:
- swap the first value (largest) with the last one (likely smallest)
- pop the values property (returning the max value, what used to be the root)
Heap Down / Sink Down:
  - parent index is at 0 (root)
  - Find the index of left child (2*index + 1) - ensure not out of bounds
  - Find the index of right child (2*index + 2) - ensure not out of bounds
  - if the left OR right child is greater than element, swap. 
    If left AND right child is greater, swap with LARGEST child
  - the child index you swapped to, now becomes the new parent index
  - keep looking and swapping until neither child is larger than the element.
return old root 

[41, 39, 33, 18, 27, 12]
[12, 39, 33, 18, 27]
[39, 12, 33, 18, 27]
[39, 27, 33, 18, 12]

  */
  remove() {
    let max = this.values[0];
    let last = this.values.pop();
    if (this.values.length > 0) {
      this.values[0] = last;
      this.heapDown();
    }
    // need to add the edge case where the list size is 0

    console.log(this.values);
    return max;
  }

  heapDown() {
    let index = 0; // start at root
    const length = this.values.length;
    const element = this.values[0];

    while (true) {
      let leftChildIdx = 2 * index + 1;
      let rightChildIdx = 2 * index + 2;
      let leftChild, rightChild;
      
      // swap will hold the index of the child to swap
      let swap = null;

      // check if indexes are in bound
      if (leftChildIdx < length) {
        leftChild = this.values[leftChildIdx];
        if (leftChild > element) {
          swap = leftChildIdx;
        }
      }
      if (rightChildIdx < length) {
        rightChild = this.values[rightChildIdx];
        // these crazy conditions are set to cover the case that the swap is occuring with the larger of the two children
        if (
          (swap === null && rightChild > element) ||
          (swap !== null && rightChild > leftChild)
        ) {
          swap = rightChildIdx;
        }
      }
      // if swap is never changed from null, there is no swap that is required
      if (swap === null) break;

      // now perform the actual swap at this point
      this.values[index] = this.values[swap];
      this.values[swap] = element;
      // ensure that the index is updated to be the child that was just swapped, so that we can check that child now
      index = swap;
    }
  }
}

let heap = new MaxBinaryHeap();
