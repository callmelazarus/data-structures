/*
Enqueue is adding to the queue
dequeue is removing from beginning of the queue
- either the dequeue, or enqueue will be costly with an array implementation.
This is because the adding or removing from the front of the array will require a re-indexing of the rest of the list….
    - this provides a strong argument for building a custom class for queues.



*/

// define queue

let q = []

// add to the end

q.push('first')

q.push('second')

q.push('third')

// remove from beg
// removing from the beginning requires the shifting of indexes (expensive)

q.shift()

q.shift()

q.shift()

// add to begin, remove from end
// add to beginning
// re-index the arrays again, because we need to shift thing
q.unshift('first')
q.unshift('second')
q.unshift('third')

// remove from end
q.pop()
q.pop()