class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class Queue {
  constructor() {
    this.first = null;
    this.last = null;
    this.size = 0;
  }

  /* ENQUEUE add to end
function takes value
create a new node using value passed in
if no nodes in queue, set this node to be the first and last property of queue
otherwise
set this.last.next to be the newNode
set this.last to be that newNode
increase size
return the size  
*/
  enqueue(val) {
    let newNode = new Node(val);
    if (this.size === 0) {
      this.first = newNode;
      this.last = newNode;
    } else {
      this.last.next = newNode;
      this.last = newNode;
    }
    // pre-increment will net differences than post-increment
    return ++this.size;
  }

  /* DEQUEUE remove from beginning
if there is no first property, return null
store first prop in variable
if only 1 node - set the first and last to be null
if more than 1 node -  set first prop to be the next property of first
decrement size
return value of node dequeued

-- NOTE: identical to stack pop --
  */
  dequeue() {
    if (this.size === 0) return null;
    let stored = this.first;
    if (this.size === 1) {
      this.last = null;
    } 
    this.first = this.first.next;
    
    this.size--;
    return stored.value;
  }
}

// most efficient to add to the end (remember removing from end of LL is SLOW)
// and remove from beginning

let queue = new Queue();

queue.enqueue("first");
queue.enqueue("second");
queue.enqueue("third");
