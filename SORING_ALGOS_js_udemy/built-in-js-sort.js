/* telling JS how to sort

optional comparator function used to sort

- looks at pairs of elements - determines their sort order
based on the return value
- if it returns (-) val, 'a' comes BEFORE 'b'
- if it returns (+) val, 'a' will come AFTER 'b'
- if it returns 0, 'a' and 'b' are the same

*/

// will result in an ascending

function numberCompare(a, b) {
  return a - b;
}

[5, 26, 1, 4].sort(numberCompare);

function compareByLen(s1, s2) {
  return s1.length - s2.length;
}

["steele", "colt", "data structures", "algo"];
