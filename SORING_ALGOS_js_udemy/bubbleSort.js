/*
Swap two values, comparing the two, bubbling up the 
largest value to the top.

PSUEDOCODE
This is a unique setup, where we are going from the last value to the first in the outer loop (different from HR lesson)

- loop from var i, from the end of array to beginning
- start an inner loop with var j, from begin until i-1
- if arr[j] is greater than arr[j+1], swap values
- return sorted array

Time: O(n^2)

IF LIST is ALMOST SORTED. One way to make that more efficient, is a check to see if a swap had already happened. 
Nearly sorted arrays can be sorted very efficiently as a result, with the noSwap variable listed below

*/

// process that has values go from end to beginning
function bubbleSort(arr) {
  let noSwaps;
  for (let i = arr.length; i > 0; i--) {
    // To increase efficiency for nearly sorted data...
    // Setup noSwap to be true initially in outer loop. If this stays True - then a swap doesn't occur
    // and we can break out with the false assignment in the inner loop.
    noSwaps = true;
    for (let j = 0; j < i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        let temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
        noSwaps = false;
      }
    }
    if (noSwaps) break;
  }
  return arr;
}

let a = [37, 45, 29, 8];
let nearlySorted = [8, 1, 2, 3, 4, 5, 6, 7, 8];
bubbleSort(nearlySorted);
