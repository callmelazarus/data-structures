# Data structures Course

Udemy - Javascript Algo and Data Structures Masterclass

- Colt Steele

https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/

# Goal:

1 section a day

2 weeks to finish Data structures section (11 total)
MILESTONE: Finish data structures by the end of this month

## Feb

**Today I worked on:**
**Reflection:**
**Lesson:**

---

## Feb

**Today I worked on:**
- bubble swap
**Reflection:**
**Lesson:**
(2) ways to swap variables.
temp = v1
v1 = v2
v2 = temp

OR

array destructuring
[v1, v2] = [v2, v1]


## Feb 10, 2023 - Fri - FINISHED DATA STRUCTURES!

**Today I worked on:**
- Dynamic programming FINISHED!
- Starting the sorting algos
**Reflection:**

**Lesson:**
Two ways to store subproblem solutions in dynamic programming (memoization - top down approach, and tabulation - bottom up approach)

## Feb 8, 2023 - Wed

**Today I worked on:**
- Continue with Dijkstra's algo
- start dynamic programming
**Reflection:**
- it's been a while since I've been doing this stuff. A lot of stuff has been going on. Kindred's launch. Job interviews. Networking.
**Lesson:**


## Feb 2, 2023 - Thur

**Today I worked on:**

- First interview with Futek. My very first real interview for a SWE position!
- graph bfs
- Dijkstra's algo intro, weighted graphs, priority queues

**Reflection:**

- starting to lose a little steam in this.

**Lesson:**

## Feb 1, 2023 - Wednesday

**Today I worked on:**

- Graph Traversal methods
  **Reflection:**
  **Lesson:**
- intialize variables outside of loops
- the 'this' keyword cannot always be readily accessed within a helper function in a class. To solve this, we can assign this.array to another array => `let array = this.array`

## Jan 31, 2023 - Tuesday

**Today I worked on:**

- Graphs (add vertex / add edge / remove vertex / remove edge)

**Reflection:**

- it is helpful to compartmentalize functions

**Lesson:**

- used filter method on a list, which takes a callback function, which can be used to remove elements of a list

## Jan 30, 2023 - Monday

**Today I worked on:**

- HashTables - implemented using arrays

**Reflection:**

- interesting how hashmaps can be built with arrays. This involves looping thru elements.
- The hashing function is very interesting. it assigns an index to the key/value pair, and stores the key/value pair in that index
  **Lesson:**
- there is a bunch to consider when building hashtables. We need to consider the speed O(1), reducing collisions, deterministic (same input will yield same output), saves data in a distributed fashion
- "string".charCodeAt(0) is a fxn that returns the UTF-16 code at the given index
- Check if something is INSIDE array - use array.includes(value) method

## Jan 26, 2023 - Thursday

**Today I worked on:**

- Heaps (storing / insert / remove)
- Priority Queue (intro /

**Reflection:**

- Heaps are no small feat... it is interesting how you can setup variables that you can then use with the conditional statements that you have setup
- simply seeing these lessons, or even typing them out, but not understanding them is not much return... just finishing this class doesn't necessarily mean I have fully understood these principles
- Pretty tired today, felt it again at 3pm. Had pizza for lunch, might be way I feel lethargic.

**Lesson:**

- the parent/child relationship (eqn) is pretty important for the heaps
- Priority Queue is an abstract DS, which means it can be modelled with an assortment of DS's. However, it is very commonly associated with Heaps, because of the efficiency of heaps.

## Jan 25, 2023 - Wednesday

**Today I worked on:**

- Tree Traversal - BFS, DFS (Preorder, postorder, Inorder)
- Heaps intro (heap is type of Tree)

**Reflection:**

- Queues - either enqueuing or dequeuing into an array will be costly, because re-indexing will always be required
- Pre order (top down) and Post order (bottom up) are so similar, but the difference is the order of the push and visit.
- The comparison between BFS and DFS in this lecture was very weak. I am reminded tho of LC problems that utilized different tree conditions

**Lesson:**

- BFS - typically uses queue (FIFO)

## Jan 24, 2023 - Tuesday

**Today I worked on:**

- Trees - Find method, and big O

**Reflection:**

- this course also is a good exposing us to OOP principles
  **Lesson:**
  BST - insert and find -> O(log N) time

## Jan 23, 2023 - Monday

**Today I worked on:**

- Stacks implementation - push/pop
- Queues - enqueue, dequeue
- Starting Trees - intro / insert

**Reflection:**

- Code here can be recursive or iterative for the tree insert method
- Refactoring can be done as needed, removing conditionals. It is a balance of readability and wordiness

**Lesson:**

- drawing out an example of a solution will be helpful in visualizing the algo

++x (pre-increment) means "increment the variable; the value of the expression is the final value"
x++ (post-increment) means "remember the original value, then increment the variable; the value of the expression is the original value"

## Jan 20, 2023 - Fri

**Today I worked on:**

- doubly linked list / pop / shift(remove) / unshift(add) / get / set / insert / remove
- start Stacks

**Reflection:**

**Lesson:**

- working with DLL, you should be resolving pointers in both direction! This is the pain related to this.
- shift - remove from beginning of node
- need to regularly consider edge cases
- optimization can occur, using the beginning half or the second half, based on the index
- Using the `insert` method requires accessing the previous node that you care about - which you can use the get method for

## Jan 19, 2023 - Thur

**Today I worked on:**

- Singly Linked List. Pop method / shift / unshift / get / set / insert / remove / print / reverse
- Doubly Linked List. intro

**Reflection:**

- a 1 hour program took about 3 hours to complete

**Lesson:**

- you want to retain the connections of the LL. Be concious of this as you build new connections

- `return !!this.push(val)` will become a bool operator (prefacing with ! or !!) It will return the opposite of the opposite. this.push will still call, but it will also return T or F

- reverse method is one of the most interesting methods

pop - removes last item in the list, and returns that item
shift - remove a new node from beginning of list
unshift - add a new node to head
get - retrieves a node by its position in the LL
set - change value of a node based on its index/position
insert - insert a node at a specific location
remove - remove a node at an index
reverse - reverse in place

## Jan 17, 2023 - Tue

**Today I worked on:**

- Singly linked list. Push method.

**Reflection:**

- Colt provides the pseudocode which is then used to build the code

**Lesson:**
