class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class Stack {
  constructor() {
    this.first = null;
    this.last = null;
    this.size = 0;
  }

  /* PUSH - add to beginning
- done at the 'front' in order for this operation to be constant time O(1)- 
Function accepts a value
create a new node with value
if NO nodes in stack, set the fifrst and last property to new node
if there is ONE node in stack, create a variable that stores the current first property on stack
reset the first property to the newly created node
set the first property o fthe node to be the previously created variable
increment size by 1
*/

  push(value) {
    let newNode = new Node(value);
    if (!this.first) {
      this.first = newNode;
      this.last = newNode;
    } else {
      let temp = this.first;
      this.first = newNode;
      this.first.next = temp;
    }
    return ++this.size;
  }

  /* POP - remove last item
- done at the 'front' in order for this operation to be constant time O(1). Popping at the end of LL is O(N) - 

if no nodes in stack, return null
create a temp variable to store the first property on the stack
if there is only 1 node, set first and last property to be null
if there is more than one node, set the first property to be the next property
decrement size by 1
return the value of the node removed

*/

  pop() {
    let popped = this.first;
    if (this.size === 0) return null;
    else if (this.first === this.last) {
      this.last = null;
    }

    // 1 -> 2 -> 3
    //
    this.first = this.first.next;
    this.size--;
    return popped.value;
  }
}

let stack = new Stack()
stack.push('first')
stack.push('second')
stack.push('third')
