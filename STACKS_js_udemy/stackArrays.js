// ARRAY implementation

// adding to end, and removing from end
let stack = []

stack.push("google")

stack.push("instagram")

stack.push("youtube")

stack.pop()

stack.pop()


// adding to beg, and removing from beg

// not as efficient, when you manipulate the beginning
// re-indexing is required, and items will need to shift down

let stack = []

stack.unshift("hi")

stack.unshift("bye")

stack.shift()