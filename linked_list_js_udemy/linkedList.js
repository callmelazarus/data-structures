// collection of nodes and arrows

// nodes - piece of data - val
// references to next node = next
// singly linked list goes one way only

class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}

class SinglyLinkedLIst {
  // initialize the various properties of a SLL
  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }
  // create a new node, if there is no node currently
  // adds to existing node, at the end

  push(val) {
    // create a new node using the value passed to the function
    let newNode = new Node(val);

    // if no head in the list, set the head and tail to the newly created node
    if (this.length === 0) {
      this.head = newNode;
      this.tail = this.head;
    }

    // otherwise - set the next property of the tail to be the new node and set the tail property on the list to the newly created node
    else {
      // have the current tail pointer to point to the newNode
      this.tail.next = newNode;

      // update the tail to be the newNode
      this.tail = newNode;
    }
    // increment the length
    this.length++;

    // return the linked list
    return this;
  }

  /*
pop: 
remove last element in list. if no nodes in list, return undefined
loop thru list until you reach the tail
set the next property of the '2nd to last node'
set the tail to be the 2nd to last node
decrement length by 1
return the value of the node removed
*/

  pop() {
    // remove last element in list. if no nodes in list, return undefined
    if (!this.head) return undefined;
    // loop thru list until you reach the tail
    let current = this.head;
    let newTail = this.head;

    while (current.next) {
      // newTail will be 'lagging behind'. Current will be updated with the next node in list
      newTail = current;
      current = current.next;
    }
    // set the tail to the newTail
    this.tail = newTail;
    // set the tail to null
    this.tail.next = null;
    this.length--;

    // cover edge case when the list is 0 items long
    if (this.length === 0) {
      this.head = null;
      this.tail = null;
    }
    return current;
  }

  /* Shift - return the head, and remove the head
constant time always, no re-indexing
if no nodes, return undefined
store the current head property in a variable
set the head property to be the current head's next property
decrement length by 1
return the value of the node removed
*/
  shift() {
    if (this.length === 0) {
      return undefined;
    }
    let oldHead = this.head;
    this.head = oldHead.next;
    this.length--;
    return oldHead;
  }

  /* UNSHIFT - adding a new node to the beginning of the new list
function accepts a value
create a new node, using value passed
if no head property on list, set the head and tail to be the newly created node
otherwise set the newly created nodes next property to be the current head property on the list
set the head property on the list to be the newly created node
increment length by 1
return linked list
*/

  unshift(value) {
    let newHead = new Node(value);
    if (this.length === 0) {
      this.head = newHead;
      this.tail = this.head;
    } else {
      newHead.next = this.head;
      this.head = newHead;
    }
    this.length++;
    return this.head;
  }

  /* GET
traverse the list a certain amount of times, based on the index
function should accept an index
if index is less than 0, or >= length of list, return null
loop thru the list, until you reach the index, and return the node at the specific index (using counter)
*/

  get(index) {
    if (index < 0 || index >= this.length) return null;
    let cur = this.head;
    for (let i = 0; i <= index; i++) {
      if (i == index) {
        return cur;
      } else {
        cur = cur.next;
      }
    }
  }

  /* SET - change the value of a node based on its index/position in the LL
accepts index and value
use get function to find the specific node
if node is not found, return False
if node found - set the value of that node to be the value passed to the function, and return True
*/

  set(index, value) {
    let cur = this.get(index);
    if (cur) {
      cur.val = value;
      return true;
    } else {
      return false;
    }
  }

  /* INSERT - add a node to the LL at a SPECIFIC Location

  add node to point to after node
  update prenode to point to new node

create insert which takes inddex and value
if index is less than 0, or greater than the length, return false
if index is same as length - insert at the end (use push)
if index is at index = 0 - insert at start (use unshift)
OTHERWISE
use the get method - access the node, at the 'index - 1'
set the next property on that node, to be the new node
set the next property on the new node to be the previous next
increment length
return true
  */

  insert(index, value) {
    if (index < 0 || index > this.length) return false;
    else if (index === this.length) {
      this.push(value);
      return true;
    } else if (index === 0) {
      this.unshift(value);
      return true;
    } else {
      let prevNode = this.get(index - 1);
      let insertNode = new Node(value);
      insertNode.next = prevNode.next;
      prevNode.next = insertNode;
      this.length++;

      // colt's process using teamp
      // let temp = prevNode.next
      // prevNode.next = insertNode
      // insertNode.next - temp
      return true;
    }
  }


  /* REMOVE - takes and index, and removes the node at that index, and patches the LL
  [a] -> [b] -> [c] -> [d]
  [a] -> [c] -> [d]

  find the index passed on
  set the prevNode.next to the curNode.next - thus breaking the link to the node that no longer has a connection

  if index is zero or greater than length - return undefined
  if index is same as 'length - 1 ', use pop 
  if index is zero - use shift
  otherwise
  use get method to access the node at the 'index -1 '
  set the next property of that prev node - to be the next of the next node
  decrement length
  return the value of node removed
  */

  remove(index){
if (index < 0 || index >= this.length) return undefined
else if (index === this.length -1) return this.pop()
else if (index === 0) return this.shift()
else {
  let prevNode = this.get(index-1)
  let removed = prevNode.next
  prevNode.next = removed.next
  this.length--
  return removed
}
  }
  
  // additional method used to print for dev assistance
  print(){
    let arr = []
    let current = this.head
    while(current){
      arr.push(current.val)
      current = current.next
    }
    console.log(arr)
  }
  
  
    /* REVERSE - reversing LL in place
    increment thru the list, changing the LL direction
  
    swap head and tail
    create a variable named next
    create variable called prev
    create variabled called node, starting at the head
    loop thru list
    set next to be the next property of whatever node is
    set the next property on the node to be whatever prev is
    set prev to be the value of the node variable
    set the node variable to be the value of the next variable...

    [100, 200, 300, 400, 500]
PREV NODE NEXT

    */


reverse() {

  // flipping head and tail
  let node = this.head
  this.head = this.tail
  this.tail = node
  let prev = null // what tail.next will point
  let next

  for (let i = 0; i < this.length; i++){
    // save the next node for future use
    next = node.next
    // assign the next node pointer to what is the prev
    node.next = prev

    // set the prev to be the current node
    prev = node

    // set current node as the next node (what was established a few lines above)
    node = next
  }
  return this
}  

}

var list = new SinglyLinkedLIst();
list.push("HI");
list.push("BOB");
list.push("BYE");
list.push("MARY");
list.push(":)");
console.log(list);


