// collection of nodes and arrows

// nodes - piece of data - val
// references to next node = next
// doubly linked list point to next and previous node

class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
    this.prev = null;
  }
}

class DoublyLinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }

  /* PUSH - takes a value, adds to end of DLL
  create a new node, with value passed in function
  check if list is empty - head and tail to be newly creatednode

  it not, set the next property on the tail to be the node
  set the previous property on the newly created node to be the tail (point backwards)
  set the tail to be the newly created node
  increment length, and return list
    */

  push(value) {
    let node = new Node(value);
    if (this.length === 0) {
      this.head = node;
      this.tail = node;
    } else {
      this.tail.next = node;
      node.prev = this.tail;
      this.tail = node;
    }
    this.length++;
    return this;
  }

  /* POP - remove last element
    - if no head, return undefined
    store current tail in var to return later
    if length is 1, set head and tail to null
    update the tail to prev node
    update the new tail.next to be null
    decrement the length
    return the value removed
    */

  pop() {
    if (!this.head) return undefined;
    let oldTail = this.tail;
    if (this.length == 1) {
      this.head = null;
      this.tail = null;
    } else {
      this.tail = oldTail.prev;
      this.tail.next = null;
      // remove link from oldTail.prev to null
      oldTail.prev = null;
    }
    this.length--;
    return oldTail;
  }

  /* SHIFT - remove from the beginning
  if length is 0 - return undefined
  store the head property in a variable - to return later
  if length = 1. set head and tail to null
  update head to be the next of the old head
  set the newheads.prev to null
  set the oldhead.next to null
  decrement length
  return hold head
  */

  shift() {
    if (this.length == 0) return undefined;
    let oldHead = this.head;
    if (this.length == 1) {
      this.head = null;
      this.tail = null;
    } else {
      this.head = oldHead.next;
      // remove connection from head and oldHead
      this.head.prev = null;
      oldHead.next = null;
    }
    this.length--;
    return oldHead;
  }

  /* UNSHIFT - add to the first element
    create new node, with value passed to the function
    if the length is 0. set head and tail to be newNode
    otherwise
    point the head.prev to the newNode
    set the next property of the new node to be the head
    update the head to be the newNode
    increment the length
    return the list
    */
  unshift(value) {
    let newHead = new Node(value);
    if (this.length === 0) {
      // or if (!this.head) {
      this.head = newHead;
      this.tail = newHead;
    } else {
      // create connection between existing head to this new node
      this.head.prev = newHead;
      newHead.next = this.head;

      this.head = newHead;
    }
    this.length++;
    return this;
  }

  /* GET - accessing a node by its position
  You can optimize the get method based on the index and the length
  if index is < 0, or >= length - return null
  Intialize some kind of counter
  if index is <= half of list:
  - loop thru list, starting at head
  - return node once its found
  if index is > half the length of list
  - loop thru list from tail, and go to middle
  return the node once it is found
    */

  get(index) {
    if (index < 0 || index >= this.length) return null;
    let counterFirstHalf = 0;
    let n = this.length;
    let counterLastHalf = n - 1;

    if (index <= Math.floor(n / 2)) {
      let cur = this.head;
      while (this.head) {
        if (counterFirstHalf == index) {
          return cur;
        }
        cur = cur.next;
        counterFirstHalf++;
      }
    } else {
      let cur = this.tail;
      while (this.tail) {
        if (counterLastHalf == index) {
          return cur;
        }
        cur = cur.prev;
        counterLastHalf--;
      }
    }
  }

  /* SET - replaces the value of a node in the DLL
create a variable which the result of the get method, at the index passed
- if the get method returns a valid node - set the value of that node
to the value passed to the function
- return TRUE
otherwise
return False

easy to write, bc get is already implemented
  */

  set(index, value) {
    if (this.get(index) != null) {
      let setNode = this.get(index);
      setNode.val = value;
      return true;
    } else return false;
  }

  /* INSERT - add a node to DLL at a index
Optimization
create new node
have the new node point to the node within DLL
have the prev node point to new node
have the new node

in index is less than 0, or >= length - return false
if index = 0 - use unshift
if index = length - use push (if index = length, you are adding a new tail)
otherwise
use the get method to access `index - 1`
set the next and prev prop on the correct nodes to link everything together
increment length
return true
*/

  insert(index, value) {
    if (index < 0 || index >= this.length) return false;
    if (index == 0) {
      this.unshift(value);
      return true;
    } else if (index == this.length) {
      this.push(value);
      return true;
    } else {
      let prevNode = this.get(index - 1);
      let newNode = new Node(value);
      // alt solution would involve creating a prevNode

      newNode.next = prevNode.next;
      prevNode.next.prev = newNode;

      prevNode.next = newNode;
      newNode.prev = prevNode;
    }
    this.length++;
    return true;
  }

  /* REMOVE - removes node at an index
if index < 0 or >= length, return undefined
if index = 0 - use shift
if index = length -1 - use pop (if index = length-1, then you are removing the last element)
otherwise
- use GET method to retrieve the item to be removed
update the next and the prev prop to remove the found node from the list
set the next and prev of the found node to be null
decrement length
return removed node
  */
  remove(index) {
    if (index < 0 || index >= this.length) return undefined;
    if (index === 0) {
      this.shift();
      return true;
    } else if (index === this.length - 1) {
      this.pop();
      return true;
    } else {
      let remNode = this.get(index);
      let prevNode = remNode.prev;
      let nextNode = remNode.next;

      prevNode.next = nextNode;
      nextNode.prev = prevNode;

      remNode.next = null;
      remNode.prev = null;

      this.length--;
      return remNode;
    }
  }
}

list = new DoublyLinkedList();
list.push(0);
list.push(1);
list.push(2);
list.push(3);
list.push(4);
