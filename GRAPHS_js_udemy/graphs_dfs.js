class Graph {
  constructor() {
    this.adjacencyList = {};
  }

  addVertex(vertex) {
    if (!this.adjacencyList[vertex]) {
      this.adjacencyList[vertex] = [];
    }
  }

  addEdge(v1, v2) {
    this.adjacencyList[v1].push(v2);
    this.adjacencyList[v2].push(v1);
  }

  removeEdge(v1, v2) {
    this.adjacencyList[v1] = this.adjacencyList[v1].filter((v) => v !== v2);
    this.adjacencyList[v2] = this.adjacencyList[v2].filter((v) => v !== v1);
  }

  removeVertex(vertex) {
    let vList = this.adjacencyList[vertex];
    for (let v of vList) {
      this.removeEdge(vertex, v);
    }
    delete this.adjacencyList[vertex];
  }

  /* DFS - Recursive
Function should accept a starting vertex
create a list to store the end result - to be returned at the very end
Create an object to store visited vertices
Create helper function which accepts a vertex
	Helper function should return early if vertex empty
	Helper function should place the vertex it accepts into visited object, and push that vertex into the result array
	Loop over all values in the adjacnecyList for that vertex
	if any of those values have not been visited, recursively invoke the helper fxn with that vertex
Invoke the helper fxn with starting vertex
Return the result array

['A', 'B', 'D', 'E', 'C', 'F']

*/

  dfsRecursive(start) {
    let res = [];
    let visited = {};
    // strange property of 'this' keyword in JS
    const adjacencyList = this.adjacencyList;
    function dfs(vertex) {
      // base case
      if (!vertex) return null;
      visited[vertex] = true;
      res.push(vertex);
      // console.log(adjacencyList[vertex])
      adjacencyList[vertex].forEach((neighbor) => {
        // if we haven't visited a neighbor, we will recursively call it
        if (!visited[neighbor]) {
          return dfs(neighbor);
        }
      });
    }
    dfs(start);
    return res;
  }

  /* DFS Iterative
function accepts starting node
Create a stack to help keep track of vertices (use array)
Create a list to store the end result, to be returned at end
Create an object to store visited vertices
Add the starting vertex to the stack and mark it visited
While stack has something in it:
  pop the next vertex from stack
  if the vertex hasn't been visited yet:
    mark as visited
    add to result list
    push all neighbors into stack

['A', 'C', 'E', 'F', 'D', 'B']

*/
  dfsIterative(start) {
    let stack = [start];
    let res = [];
    let visited = {};
    visited[start] = true;
    let adjacencyList = this.adjacencyList;
    let curVertex;

    while (stack.length) {
      curVertex = stack.pop();
      res.push(curVertex);
      // check out other neighbors
      adjacencyList[curVertex].forEach((neighbor) => {
        if (!visited[neighbor]) {
          visited[neighbor] = true;
          stack.push(neighbor);
        }
      });
    }
    return res;
  }
}

let g = new Graph();

g.addVertex("A");
g.addVertex("B");
g.addVertex("C");
g.addVertex("D");
g.addVertex("E");
g.addVertex("F");

g.addEdge("A", "B");
g.addEdge("A", "C");
g.addEdge("B", "D");
g.addEdge("C", "E");
g.addEdge("D", "E");
g.addEdge("D", "F");
g.addEdge("E", "F");

// recursive("A") ['A', 'B', 'D', 'E', 'C', 'F']
// iterative("A") ['A', 'C', 'E', 'F', 'D', 'B']

//          A
//        /   \
//       B     C
//       |     |
//       D --- E
//        \   /
//          F
