class Graph {
  constructor() {
    this.adjacencyList = {};
  }

  addVertex(vertex) {
    if (!this.adjacencyList[vertex]) {
      this.adjacencyList[vertex] = [];
    }
  }

  addEdge(v1, v2) {
    this.adjacencyList[v1].push(v2);
    this.adjacencyList[v2].push(v1);
  }

  removeEdge(v1, v2) {
    this.adjacencyList[v1] = this.adjacencyList[v1].filter((v) => v !== v2);
    this.adjacencyList[v2] = this.adjacencyList[v2].filter((v) => v !== v1);
  }

  removeVertex(vertex) {
    let vList = this.adjacencyList[vertex];
    for (let v of vList) {
      this.removeEdge(vertex, v);
    }
    delete this.adjacencyList[vertex];
  }

  /* Breadth First Search - very similar to the iterative DFS

- Function accepts a starting vertex
- create a queue/array and place starting vertex there
- create an array to store the nodes visited
- create an object to store nodes visited
- mark starting vertex as visited
- Loop as long as there is anyting in the queue
  - remove the first vertex from the queue and push it into the array that stores nodes visited
  - loop over each vertex in the adjacency list for the vertex you are visiting.
  - if it is not inside the object that stores nodes visited, mark it as visited and enqueue that vertex

*/

  bfs(start) {
    const queue = [start];
    let res = [];
    let visited = {};
    let curVertex
    visited[start] = true
    while(queue.length > 0){
      curVertex = queue.shift()
      res.push(curVertex)

      this.adjacencyList[curVertex].forEach((neighbor) => {
        // if the neighbor has not been visited (doesn't exist in visited)
        if (!visited[neighbor]) {
          visited[neighbor] = true
          queue.push(neighbor)
        }
      })
    }
    return res
  }
}

let g = new Graph();

g.addVertex("A");
g.addVertex("B");
g.addVertex("C");
g.addVertex("D");
g.addVertex("E");
g.addVertex("F");

g.addEdge("A", "B");
g.addEdge("A", "C");
g.addEdge("B", "D");
g.addEdge("C", "E");
g.addEdge("D", "E");
g.addEdge("D", "F");
g.addEdge("E", "F");

// dfs recursive("A") ['A', 'B', 'D', 'E', 'C', 'F']
// dfs iterative("A") ['A', 'C', 'E', 'F', 'D', 'B']
// bfs ['A', 'B', 'C', 'D', 'E', 'F']
//          A
//        /   \
//       B     C
//       |     |
//       D --- E
//        \   /
//          F
