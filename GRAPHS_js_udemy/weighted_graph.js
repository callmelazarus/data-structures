/* Weighted graph

{
  "A" : [{node: "B", weight: 10}, 
  {node: "C", weight: 10}
]
}

*/

class WeightedGraph {
  constructor() {
    this.adjacencyList = {};
  }
  addVertex(vertex) {
    if (!this.adjacencyList[vertex]) this.adjacencyList[vertex] = [];
  }
  addEdge(v1, v2, weight) {
    this.adjacencyList[v1].push({ node: v2, weight: weight });
    this.adjacencyList[v2].push({ node: v1, weight: weight });
  }
}
/* Priority Queue - which will provide the smallest value
This is one niave approach for a PQ
sorting is O(N*log(N))
*/
class PriorityQueue {
  constructor() {
    this.values = [];
  }
  enqueue(val, priority) {
    this.values.push({ val, priority });
    this.sort();
  }
  dequeue() {
    return this.values.shift();
  }
  sort() {
    class PriorityQueue {
      constructor() {
        this.values = [];
      }
      enqueue(val, priority) {
        this.values.push({ val, priority });
        this.sort();
      }
      dequeue() {
        return this.values.shift();
      }
      sort() {
        this.values.sort((a, b) => a.priority - b.priority);
      }
    }


  }
}


