class Graph {
  constructor() {
    this.adjacencyList = {};
  }

  /* ADD vertex
    accepts name of vertex
    add a key to the adj list with the name of the vertex and set its value to be and empty array
    */

  addVertex(vertex) {
    if (!this.adjacencyList[vertex]) {
      this.adjacencyList[vertex] = [];
    }
  }

  /* ADD edge
- function accepts two vertices - v1, v2
- find adj list, the key of v1, and push v2 to that array
- finds adj list, the key of v2, and push v1 to array
- This is an undirected graph - both in direction
  */
  addEdge(v1, v2) {
    this.adjacencyList[v1].push(v2);
    this.adjacencyList[v2].push(v1);
  }

  /* REMOVE edge
- accepts two vertices, v1 and v2
- function should re-assign the key of v1 to be an array that 
DOESN'T contain v2
- function should re-assign the key of v2 to be an array that 
DOESN'T contain v1
  */
  removeEdge(v1, v2) {
    this.adjacencyList[v1] = this.adjacencyList[v1].filter((v) => v !== v2);
    this.adjacencyList[v2] = this.adjacencyList[v2].filter((v) => v !== v1);
  }

  /* REMOVE vertex
- accepts vertex to remove
- function needs to loop as long as there are other vertices in the adj list for that vertex
- inside loop, call removeEdge fxn with the vertex we are removing and any values in the adj list for that vertex
- delete key in the adj list
  */
  removeVertex(vertex) {
    let vList = this.adjacencyList[vertex];
    for (let v of vList) {
      this.removeEdge(vertex, v);
    }
    delete this.adjacencyList[vertex]
  }
}
let g = new Graph();
g.addVertex("Dallas");
g.addVertex("Tokyo");
g.addVertex("Aspen");
g.addVertex("Los Angeles");
g.addVertex("Hong Kong");
g.addEdge("Dallas", "Tokyo");
g.addEdge("Dallas", "Aspen");
g.addEdge("Hong Kong", "Tokyo");
g.addEdge("Hong Kong", "Dallas");
g.addEdge("Los Angeles", "Hong Kong");
g.addEdge("Los Angeles", "Aspen");
