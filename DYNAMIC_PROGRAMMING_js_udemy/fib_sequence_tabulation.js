/* 
writing a Fibonacci sequence - Tabluation

Tabulation...
typically uses iteration
Better space complexity than memoization


BIG O
O(N) - linear

Reminder - we are growing the loop based on the 

The solution below is a BOTTOM UP approach
We are generating solutions, as we consider solutions of smaller number, up until we reach 'n'

*/

function Fib(number) {
  if (number <= 2) {
    return 1;
  }

  // build an array of solution
  // Javascript arrays are dynamically sized, we don't need to define the size of the array

  let fibNums = [0, 1, 1];

  // loop thru the values until you reach number

  for (let i = 3; i <= number; i++) {
    fibNums[i] = fibNums[i - 1] + fibNums[i - 2];
  }

  return fibNums[number];
}

Fib(1);
Fib(50);
