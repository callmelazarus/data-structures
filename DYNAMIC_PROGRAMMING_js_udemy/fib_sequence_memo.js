/* 
writing a Fibonacci sequence - memoization


BIG O
O(N) - linear
Reminder - accessing a value in a list is O(C)

The solution below is a TOP DOWN approach

*/



function Fib(number, memo = []) {
  //introduce memo-ization
  // The memo will actually associate the solution with the index of the memo

  // if there is a solution already stored, let's return it
  if (memo[number] !== undefined) return memo[number];

  if (number <= 2) {
    return 1;
  }

  // store the solutions you've already picked up
  let res = Fib(number - 1, memo) + Fib(number - 2, memo);
  memo[number] = res;
  return res
}

Fib(1);
Fib(50);
