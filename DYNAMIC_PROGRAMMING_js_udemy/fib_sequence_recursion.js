/* 
writing a Fibonacci sequence - recursion

Fib(n) = Fib(n-1) + Fib(n-2)
Fib(2) is 1
Fib(1) is 1

BIG O
O (2^n) -> Exponential - really bad...

We are constantly repeating the same problem solutions 
over and over...

There is overlapping sub-problems

*/

// recursive solution

function Fib(number) {

  if(number <= 2){
    return 1
  }

return Fib(number-1) + Fib(number-2)

}

Fib(1)
