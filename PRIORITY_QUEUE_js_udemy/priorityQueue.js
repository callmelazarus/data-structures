class Node {
  constructor(val, priority) {
    this.val = val;
    this.priority = priority;
  }
}

class PriorityQueue {
  constructor() {
    this.values = [];
  }
  /* lower value, is higher priority. ie. 1 is highest priority

each node has a val and priority. Use priority to build heap.

Enqueue/insert -> accepts val and priority, makes node and puts it in the right spot

Dequeue/remove -> removes root element, returns it, re-arranges heap accodringly

Currently there is no guaranteed relationship between siblings. Addl logic could be added if needed to allow for processing this.

*/
  enqueue(val, priority) {
    const node = new Node(val, priority);
    this.values.push(node);
    this.bubbleUp();
    console.log(this.values);
  }

  bubbleUp() {
    let index = this.values.length - 1;
    const element = this.values[index];
    // stop once the index goes below 0
    while (index > 0) {
      let parentIndex = Math.floor((index - 1) / 2);
      let parent = this.values[parentIndex];
      if (element.priority >= parent.priority) break;

      this.values[parentIndex] = element;
      this.values[index] = parent;
      // need to update the index with the parentindex, to now check if that should be bubbled up as well
      index = parentIndex;
    }
  }

  dequeue() {
    let min = this.values[0];
    let last = this.values.pop();
    if (this.values.length > 0) {
      this.values[0] = last;
      this.heapDown();
    }
    // need to add the edge case where the list size is 0

    console.log(this.values);
    return min;
  }

  heapDown() {
    let index = 0; // start at root
    const length = this.values.length;
    const element = this.values[0];

    while (true) {
      let leftChildIdx = 2 * index + 1;
      let rightChildIdx = 2 * index + 2;
      let leftChild, rightChild;

      // swap will hold the index of the child to swap
      let swap = null;

      // check if indexes are in bound
      if (leftChildIdx < length) {
        leftChild = this.values[leftChildIdx];
        if (leftChild.priority < element.priority) {
          swap = leftChildIdx;
        }
      }
      if (rightChildIdx < length) {
        rightChild = this.values[rightChildIdx];
        // these crazy conditions are set to cover the case that the swap is occuring with the larger of the two children
        if (
          (swap === null && rightChild.priority < element.priority) ||
          (swap !== null && rightChild.priority < leftChild.priority)
        ) {
          swap = rightChildIdx;
        }
      }
      // if swap is never changed from null, there is no swap that is required
      if (swap === null) break;

      // now perform the actual swap at this point
      this.values[index] = this.values[swap];
      this.values[swap] = element;
      // ensure that the index is updated to be the child that was just swapped, so that we can check that child now
      index = swap;
    }
  }
}

let ER = new PriorityQueue();

ER.enqueue("Common Cold", 6);
ER.enqueue("Gunshot", 1);
ER.enqueue("High Fever", 4);
ER.enqueue("broken arm", 2);
ER.enqueue("glass in foot", 3);
