/* Hashing
- implementation here is using arrays
- hash function: In order to look up values by key, we need to convert keys into valid array indices

Hash fxn concept: Index in an array that will be associated with key/value pair that we pass in
*/

/* INTRO HASH FUNCTION - Mark 1

hashes a string
Not constant time - linear in key length
not sufficiently random
*/
function hash_v1(key, arrayLen) {
  let total = 0;
  for (let char of key) {
    // map 'a' to 1, 'b' to c, ...
    // subtracting the 96 will convert the UTF-16 value to a number more easily processesable
    let value = char.charCodeAt(0) - 96;
    total = (total + value) % arrayLen;
  }
  return total;
}

/* Hash Function - Mark 2
speed it up
add more randomness to it

use prime numbers to minimize clashing/collisions
*/
function hash_v2(key, arrayLen) {
  let total = 0;
  // use of prime #'s reduces clashing
  let WEIRD_PRIME = 31;
  for (let i = 0; i < Math.min(key.length, 100); i++) {
    let char = key[i];
    let value = char.charCodeAt(0) - 96;
    total = (total * WEIRD_PRIME + value) % arrayLen;
  }
  return total;

}

hash_v2("pink", 9);
