/* Hashing
- implementation here is using arrays
- hash function: In order to look up values by key, we need to convert keys into valid array indices
Hash fxn concept: Index in an array that will be associated with key/value pair that we pass in

*/

class HashTable {
  constructor(size = 53) {
    this.keyMap = new Array(size);
  }

  // This is just one variation of a hashing function
  hash(key) {
    let total = 0;
    let WEIRD_PRIME = 31;
    for (let i = 0; i < Math.min(key.length, 100); i++) {
      let char = key[i];
      let value = char.charCodeAt(0) - 96;
      // creation of the index value, using the prime number, and modulus of the keyMap length
      total = (total * WEIRD_PRIME + value) % this.keyMap.length;
    }
    return total;
  }

  /* 
--Set-- 
accept a key and a value
hashes the key
stores the key/value pairing in hash table with seperate chaining

*/

  set(key, value) {
    let index = this.hash(key);
    // if the keymap index is empty, assign it the key/value
    // else -> establish an list with the previous key/value pair, and add new key/value pair

    // if there is nothing at that index, initialize an empty list into that index value
    if (!this.keyMap[index]) {
      this.keyMap[index] = [];
    }
    // ALWAYS push the key value pair as an array into that keyMap
    this.keyMap[index].push([key, value]);
    return this.keyMap[index];
  }

  /*
--Get--
accepts a key
hashes the key
retrieves the key value pair in hash table (considering seperate chaining)
if key not found, return undefined
*/

  get(key) {
    let index = this.hash(key);
    if (this.keyMap[index]) {
      let result = this.keyMap[index];
      for (let item of result) {
        if (item[0] === key) {
          return item;
        }
      }
    }
    return undefined;
  }

  /* Keys
  Loop thru hash table array - return array of keys in table
  * typically return just unique keys
  */

  keys() {
    let keyList = [];
    for (let items of this.keyMap) {
      if (items) {
        for (let subItems of items) {
          if (!keyList.includes(subItems[0])) {
            keyList.push(subItems[0]);
          }
        }
      }
    }
    return keyList;
  }

  /* Values
 Loop thru hash table array - return array of values in table
 * typically return just unique values
 */
  values() {
    let valuesList = [];
    for (let items of this.keyMap) {
      if (items) {
        for (let subItems of items) {
          if (!valuesList.includes(subItems[1])) {
            valuesList.push(subItems[1]);
          }
        }
      }
    }
    return valuesList;
  }
}

let ht = new HashTable();
ht.set("hi", "bye");
ht.set("dog", "cool");
ht.set("fish", "cool");
ht.set("cat", "fine");
ht.set("cat", "garfield");
ht.set("i love", "pizza");
